README
------
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Greek, edition 1.3. See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documetation of the annotation principles.

The present Greek data result from an update and an extension of the Greek part of the [PARSEME 1.2 corpus](http://hdl.handle.net/11234/1-3367). For the changes with respect to the 1.2 version, see the change log below.

The raw corpus from edition 1.2 is not released in the present directory, but can be downloaded from a [dedicated page](https://gitlab.com/parseme/corpora/-/wikis/Raw-corpora-for-the-PARSEME-1.2-shared-task).

Source Corpora
---------------------------
*	EL-POLYTROPON corpus: subpart of the POLYTROPON [corpus](https://www.researchgate.net/publication/367233460_Annotating_Greek_VMWEs_in_running_text_a_piece_of_cake_or_looking_for_a_needle_in_a_haystack) of Modern Greek. 
*	GDT: subpart of the [Greek Dependency Treebank](http://gdt.ilsp.gr/).

Format
-------------

The data are in [.cupt](http://multiword.sourceforge.net/cupt-format) format. The following tagsets have been used:

* column 4 (UPOS): [UD POS-tags] (http://universaldependencies.org/u/pos) version 2.5.
* column 5 (XPOS): GDT-UD tagset. BOth tagsets are equivalent.
* column 6 (FEATS): [UD features](http://universaldependencies.org/u/feat/index.html) version 2.5 (as of March 2020).
* column 8 (DEPREL) [UD dependency relations](http://universaldependencies.org/u/dep) version 2.5 (as of March 2020).
column 11 (PARSEME:MWE): [PARSEME VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.2/?page=030_Categories_of_VMWEs) version 1.2.

Text genres, origins and annotations
--------------------------------------------------------------
The corpus comprises textual data (mostly newswire texts) from various online sources (EL wikipedia, extracts from newspaper, magazine and news portals articles. Sources of textual data are: kathimerini, tovima, tanea, avgi, protothema, in.gr, iefimerida, efsyn, protagon, capital, newsit, espresso). The GDT also contains parliament debates.

The VMWE annotations (column 11) were performed by multiple annotators. The following [categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.2/?page=030_Categories_of_VMWEs) are annotated: VID, LVC.full, LVC.cause, VPC.full.

The lemmatization and morpho-syntactic annotation of the sentences originating from the GDT was performed manually. For the rest of the data, annotations were performed automatically using UDPipe relied on the model `greek-gdt-ud-2.5-191206`.

Tokenization was performed automatically

* Contractions: Most contractions are kept as a single unit (not-split).  Only the forms _στου_ (_στης_, _στον_, _στη_, _στην_, _στο_, _στων_, _στους_, _στις_, _στα_) are split as two tokens _σ_ and _του_ (_της_, _τον_, _τη_, _την_, _το_, _των_, _τους_, _τις_, _τα_).

Companion raw corpus
------------------------------------------
The raw data were drawn from the Greek section of the Leipzig Corpus collection (https://wortschatz.uni-leipzig.de/en/download/) (Goldhahn et al., 2012), i.e., the news and wikipedia subcorpora, as well as a sub-part of the CoNLL 2017 shared task raw corpora (http://hdl.handle.net/11234/1-1989).

Licence
-------
The VMWEs annotations (column 11) are distributed under the terms of the [CC-BY v4 license](https://creativecommons.org/licenses/by/4.0/).
The lemmas, POS-tags, morphological and features (columns 1-6), are distributed under the terms of the [CC-BY-NC-SA v4.0](https://creativecommons.org/licenses/by-sa/4.0/) for the sentences from the GDT-UD. For the rest of the sentences, annotations are distributed under the terms of the [GNU GPL v.3](https://www.gnu.org/licenses/gpl.html).

Authors
-------
Anna Kanelopoulos annotated new data in version 1.3. Voula Giouli made corrections towards enhancing the existing annotations. Voula Giouli, Vassiliki Foufi, Aggeliki Fotopoulou, and Stella Markantonatou annotated new data in version 1.2. Stella Papadelli and Sevasti Louizou contributed to the annotations in versions 1.0 and 1.1 respectively.

Change log
-------------------
- **2022-10-03**:
	- Version 1.3 of the corpus is getting ready for the release on LINDAT.
	- Changes with respect to version 1.2 of the corpus are the following:
		- correcting errors (after consistency checks)
		- enhancing the corpus with new data.
		- updating the README file.
- **2020-07-09**:
	- [Version 1.2](http://hdl.handle.net/11234/1-3367) of the corpus was released on LINDAT.
	- Changes with respect to the 1.1 version are the following:
		- annotating new files
		- fixing known bugs in previous annotations
		- providing a companion raw corpus, automatically annotated for morpho-syntax
		- updating the README file.
- **2018-04-30**:
	- [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT.
	- Changes with respect to the 1.0 version are the following:
		- extending the corpus with a sub-part of the GDT
		- updating the existing VMWE annotations to comply with PARSEME guidelines eidtion 1.1.
		- updating the READEME file.
- ** 2017-01-20**:
	- [Version 1.0](http://hdl.handle.net/11372/LRT-2282) of the corpus was released on LINDAT.

References
----------
Giouli, V., Foufi, V., and Fotopoulou, A. (2019). Annotating Greek VMWEs in running text: a piece of cake or looking for a needle in a haystack? In Proceedings of the 13th International Conference on Greek Linguistics, London, UK. 

Goldhahn, D., Eckart, T., & Quasthoff, U. (2012). Building Large Monolingual Dictionaries at the Leipzig Corpora Collection: From 100 to 200 Languages. In: Proceedings of the 8th International Language Ressources and Evaluation (LREC'12).


Contact
voula@athenarc.gr
